import sys, json
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Gdk
import socket
import threading

import random

class MessageBubble(Gtk.Box):
    def __init__(self, message, sender, color):
        super().__init__(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.set_homogeneous(False)

        self.label = Gtk.Label(label=message)
        self.label.set_line_wrap(True)

        self.sender_label = Gtk.Label(label=sender + ":")
        self.sender_label.set_line_wrap(True)
        self.pack_start(self.sender_label, False, False, 0)
        self.pack_start(self.label, False, False, 0)

        # Apply CSS class
        self.get_style_context().add_class("message-bubble")
        self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(*hex_to_rgb(color)))
        self.sender_label.get_style_context().add_class("sender-label")
        self.label.get_style_context().add_class("message-text")

class ChatClient(Gtk.Window):
    def __init__(self):
        super().__init__(title="GTK Socket Chat")
        self.set_size_request(400, 700)
        self.color = generate_random_color(0.5, 0.5)
        print(self.color)

        # Load CSS
        self.load_css()

        self.layout = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.layout_text = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.add(self.layout)

        self.message_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.chat_history = Gtk.ScrolledWindow()
        self.chat_history.set_vexpand(True)
        self.chat_history.add(self.message_box)
        self.layout.pack_start(self.chat_history, True, True, 0)
        
        self.separator = Gtk.Separator()
        self.separator.get_style_context().add_class("separator")
        self.layout.pack_start(self.separator, False, True, 0)
        self.layout.pack_start(self.layout_text, False, True, 0)



        self.textview = Gtk.TextView()
        self.textview.get_style_context().add_class("textview")
        self.layout_text.pack_start(self.textview, True, True, 0)

        self.send_button = Gtk.Button(label="Send")
        self.send_button.get_style_context().add_class("send_button")
        self.send_button.connect("clicked", self.on_send_clicked)
        self.layout_text.pack_start(self.send_button, False, False, 0)
        
        self.get_style_context().add_class("window")

        self.connect_to_server()

    def load_css(self):
        css_provider = Gtk.CssProvider()
        css_provider.load_from_data("""
            
            .window, .textview *, .send_button {
                background-color: white;
                background: white;
                color: black;
                padding: 6px;
            }
            .message-bubble {
                padding: 6px;
                border-radius: 10px;
                margin-left: 6px;
                margin-top: 6px;
                margin-right: 6px;
            }
            .sender-label {
                color: #ffffff;
                font-weight: bold;
                margin-right: 6px;
            }
            .message-text {
                color: #ffffff;
            }
            .separator {
                background: #333;
            }
        """)
        screen = Gdk.Screen.get_default()
        context = Gtk.StyleContext()
        context.add_provider_for_screen(screen, css_provider,
                                         Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def connect_to_server(self):
        self.server_address = (sys.argv[1], int(sys.argv[2]))
        print(sys.argv[1], int(sys.argv[2]))
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            self.socket.connect(self.server_address)
            self.receive_thread = threading.Thread(target=self.receive_message)
            self.receive_thread.start()
        except ConnectionRefusedError:
            self.display_message("Failed to connect to the server.")

    def display_message(self, message, sender, color):
        bubble = MessageBubble(message, sender, color)
        self.message_box.pack_start(bubble, False, False, 0)
        self.show_all()

    def receive_message(self):
        while True:
            message = self.socket.recv(1024).decode('utf-8')
            if message:
                data = json.loads(message)
                GLib.idle_add(self.display_message, data["message"], data["name"], data["color"])
            else:
                self.display_message("Disconnected from the server.", "ERROR", "#ff0000")
                self.socket.close()
                break

    def get_text_from_textview(self):
        buffer = self.textview.get_buffer()
        start_iter = buffer.get_start_iter()
        end_iter = buffer.get_end_iter()
        text = buffer.get_text(start_iter, end_iter, True)
        return str(text.strip())

    def on_send_clicked(self, button):
        message = self.get_text_from_textview()
        data = {}
        data["message"] = message
        data["name"] = socket.gethostname()
        data["color"] = self.color
        if message:
            try:
                self.socket.sendall(json.dumps(data).encode('utf-8'))
                self.textview.get_buffer().set_text("")
                self.display_message(message, "You", self.color)
            except Exception as e:
                self.display_message("Failed to send message. Disconnected from the server." + str(e), "ERROR", "#ff0000")

def generate_random_color(lightness, saturation):
    hue = random.random()  # Random hue
    # Convert HSL to RGB without using any imports
    c = (1 - abs(2 * lightness - 1)) * saturation
    x = c * (1 - abs((hue * 6) % 2 - 1))
    m = lightness - c / 2
    if 0 <= hue < 1/6:
        rgb = (c, x, 0)
    elif 1/6 <= hue < 2/6:
        rgb = (x, c, 0)
    elif 2/6 <= hue < 3/6:
        rgb = (0, c, x)
    elif 3/6 <= hue < 4/6:
        rgb = (0, x, c)
    elif 4/6 <= hue < 5/6:
        rgb = (x, 0, c)
    else:
        rgb = (c, 0, x)
    # Adjust RGB values and return as integers
    rgb_int = tuple(int((r + m) * 255) for r in rgb)
    # Convert to hexadecimal format
    hex_color = "#{0:02x}{1:02x}{2:02x}".format(*rgb_int)
    return hex_color

def hex_to_rgb(hex_color):
    hex_color = hex_color.lstrip('#')
    return tuple(int(hex_color[i:i+2], 16) / 255.0 for i in (0, 2, 4))


if __name__ == "__main__":
    win = ChatClient()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()

