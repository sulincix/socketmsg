#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8000
#define MAX_CLIENTS 100
#define BUFFER_SIZE 8192

int main() {
    int server_fd, new_socket, max_clients = MAX_CLIENTS;
    int client_sockets[MAX_CLIENTS] = {0}; // Initialize array with zeros
    fd_set readfds;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    char buffer[BUFFER_SIZE] = {0};
    
    // Creating server socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
    
    // Bind
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    
    // Listen
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    
    printf("Server listening on port %d\n", PORT);
    
    while (1) {
        FD_ZERO(&readfds);
        FD_SET(server_fd, &readfds);
        int max_sd = server_fd;
        
        for (int i = 0; i < max_clients; i++) {
            int sd = client_sockets[i];
            if (sd > 0) {
                FD_SET(sd, &readfds);
                if (sd > max_sd) {
                    max_sd = sd;
                }
            }
        }
        
        int activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
        if ((activity < 0) && (errno != EINTR)) {
            printf("select error\n");
        }
        
        if (FD_ISSET(server_fd, &readfds)) {
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            printf("New connection, socket fd is %d, ip is : %s, port : %d \n",
                new_socket,
                inet_ntoa(address.sin_addr),
                ntohs(address.sin_port));
            
            // Add new socket to array of sockets
            for (int i = 0; i < max_clients; i++) {
                if (client_sockets[i] == 0) {
                    client_sockets[i] = new_socket;
                    break;
                }
            }
        }
        
        for (int i = 0; i < max_clients; i++) {
            int sd = client_sockets[i];
            int read_size;
            if (FD_ISSET(sd, &readfds)) {
                read_size = read(sd, buffer, BUFFER_SIZE);
                if (read_size == 0) {
                    // Client disconnected
                    printf("Client disconnected\n");
                    close(sd);
                    client_sockets[i] = 0;
                } else {
                    // Echo message back to all other clients
                    // printf("Received: %s\n", buffer);
                    for (int j = 0; j < max_clients; j++) {
                        int dest_sd = client_sockets[j];
                        if (dest_sd != 0 && dest_sd != sd) {
                            send(dest_sd, buffer, read_size, 0);
                        }
                    }
                    memset(buffer, 0, sizeof(buffer));
                }
            }
        }
    }
    
    return 0;
}

